from typing import List, Dict, Union

from Common.patent_parser import PatentParser


class Patent:
    def __init__(self, patent_link: str, patent_parser: PatentParser) -> None:
        self.__patent_link = patent_link
        self.__patent_parser = patent_parser

        self.patent_link = self.__patent_link

    @property
    def patent_link(self) -> str:
        return self.__patent_link

    @patent_link.setter
    def patent_link(self, patent_link: str) -> None:
        self.__patent_link = patent_link
        self.__patent_parser.set_patent_link(patent_link)

    @property
    def patent_parser(self) -> PatentParser:
        return self.__patent_parser

    @patent_parser.setter
    def patent_parser(self, patent_parser: PatentParser) -> None:
        self.__patent_parser = patent_parser
        self.__patent_parser.set_patent_link(self.__patent_link)

    def get_id(self) -> str:
        """
        Получить уникальный идентификатор.
        :return: Уникальный идентификатор в строковом представлении.
        """
        return self.__patent_parser.get_id()

    def get_title(self) -> str:
        """
        Получить заголовок.
        :return: Заголовок.
        """
        return self.__patent_parser.get_title()

    def get_assignee(self) -> str:
        """
        Получить правопреемника.
        :return: Правопреемник.
        """
        return self.__patent_parser.get_assignee()

    def get_authors(self) -> List[str]:
        """
        Получить авторов.
        :return: Список авторов.
        """
        return self.__patent_parser.get_authors()

    def get_publications(self) -> List[str]:
        """
        Получить список дат публикаций.
        :return: Список дат в строковом представлении.
        """
        return self.__patent_parser.get_publications()

    def get_abstract(self) -> str:
        """
        Получить аннотацию.
        :return: Текст аннотации.
        """
        return self.__patent_parser.get_abstract()

    def get_description(self) -> str:
        """
        Получить описание.
        :return: Текст описания.
        """
        return self.__patent_parser.get_description()

    def get_claims(self) -> str:
        """
        Получить формулы изобретения.
        :return: Текст формул изобретения.
        """
        return self.__patent_parser.get_claims()

    def get_classifications(self) -> List[str]:
        """
        Получить классы по классификации CPC.
        :return: Список названий классов.
        """
        return self.__patent_parser.get_classifications()

    def get_cited_by(self) -> List[str]:
        """
        Получить список идентификаторов патентов, цитирующих текущий патент.
        :return: Список идентификаторов патентов.
        """
        return self.__patent_parser.get_cited_by()

    def get_concepts(self) -> List[Dict[str, Union[str, int]]]:
        """
        Получить список ключевых терминов.
        :return: Список ключевых терминов с количеством употребления.
        """
        return self.__patent_parser.get_concepts()

from abc import ABC, abstractmethod

import pandas as pd

from Models.patent_model import PatentModel


class DatabaseProvider(ABC):
    def __init__(self, connection_string: str) -> None:
        """
        Инициализирует провайдер базы данных.
        :param connection_string: Строка подключения к БД.
        """
        self._connection_string = connection_string
        self._db_name = 'patent_analysis'
        self._table_name = 'patents'

    @abstractmethod
    def ensure_db_and_table_created(self) -> None:
        """
        Удостоверяет, что БД и таблица (коллекция и т.п.) созданы.
        """
        raise NotImplementedError

    @abstractmethod
    def check_if_patent_exists(self, patent_id: str) -> bool:
        """
        Проверяет, содержится ли патент в БД.
        :param patent_id: Уникальный идентификатор патента.
        :return: True, если содержится патент в БД, иначе - False.
        """
        raise NotImplementedError

    @abstractmethod
    def insert_patent(self, patent: PatentModel) -> None:
        """
        Добавляет патент в БД.
        :param patent: Патент.
        """
        raise NotImplementedError

    @abstractmethod
    def get_table_for_analysis(self, limit: int = 500) -> pd.DataFrame:
        """
        Загружает термы всех патентов и устанавливает количество их употребления в каждом патенте.
        :param limit: Количество патентов в сформированном патентном массиве.
        :return: Таблица, содержащая количество употреблений термов в каждом патенте,
        дату публикации и количество цитирований.
        """
        raise NotImplementedError

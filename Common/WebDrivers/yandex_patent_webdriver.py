import time

from Common.patent_web_driver import PatentWebDriver


class YandexPatentWebDriver(PatentWebDriver):
    def __init__(self, path_to_driver: str = 'chromedriver.exe') -> None:
        super().__init__(path_to_driver)

    def get_patent_html(self, patent_link: str) -> str:
        self._driver.get(patent_link)
        time.sleep(1)
        return self._driver.page_source

    def get_search_patents(self, patent_link) -> str:
        self._driver.get(patent_link)
        time.sleep(5)
        return self._driver.page_source

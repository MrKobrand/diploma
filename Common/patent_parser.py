from abc import ABC, abstractmethod
from typing import List, Dict, Union


class PatentParser(ABC):
    @abstractmethod
    def set_patent_link(self, patent_link: str) -> None:
        """
        Установить ссылку патента.
        :param patent_link: Ссылка на патент.
        """
        raise NotImplementedError

    @abstractmethod
    def get_id(self) -> str:
        """
        Получить уникальный идентификатор.
        :return: Уникальный идентификатор в строковом представлении.
        """
        raise NotImplementedError

    @abstractmethod
    def get_title(self) -> str:
        """
        Получить заголовок.
        :return: Заголовок.
        """
        raise NotImplementedError

    @abstractmethod
    def get_assignee(self) -> str:
        """
        Получить правопреемника.
        :return: Правопреемник.
        """
        raise NotImplementedError

    @abstractmethod
    def get_authors(self) -> List[str]:
        """
        Получить авторов.
        :return: Список авторов.
        """
        raise NotImplementedError

    @abstractmethod
    def get_publications(self) -> List[str]:
        """
        Получить список дат публикаций.
        :return: Список дат в строковом представлении.
        """
        raise NotImplementedError

    @abstractmethod
    def get_abstract(self) -> str:
        """
        Получить аннотацию.
        :return: Текст аннотации.
        """
        raise NotImplementedError

    @abstractmethod
    def get_description(self) -> str:
        """
        Получить описание.
        :return: Текст описания.
        """
        raise NotImplementedError

    @abstractmethod
    def get_claims(self) -> str:
        """
        Получить формулы изобретения.
        :return: Текст формул изобретения.
        """
        raise NotImplementedError

    @abstractmethod
    def get_classifications(self) -> List[str]:
        """
        Получить классы по классификации CPC.
        :return: Список названий классов.
        """
        raise NotImplementedError

    @abstractmethod
    def get_cited_by(self) -> List[str]:
        """
        Получить список идентификаторов патентов, цитирующих текущий патент.
        :return: Список идентификаторов патентов.
        """
        raise NotImplementedError

    @abstractmethod
    def get_concepts(self) -> List[Dict[str, Union[str, int]]]:
        """
        Получить список ключевых терминов.
        :return: Список ключевых терминов с количеством употребления.
        """
        raise NotImplementedError

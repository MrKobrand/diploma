import os

import clickhouse_connect
import pandas as pd

from Common.database_provider import DatabaseProvider
from Models.patent_model import PatentModel


class ClickhouseProvider(DatabaseProvider):
    def __init__(self, connection_string: str) -> None:
        """
        Иницилизирует провайдер БД ClickHouse.
        :param connection_string: Строка подключения к БД.
        """
        super().__init__(connection_string)
        self._client = clickhouse_connect.get_client(
            host=connection_string,
            username=os.getenv('CLICKHOUSE_USERNAME'),
            password=os.getenv('CLICKHOUSE_PASSWORD')
        )

    def ensure_db_and_table_created(self) -> None:
        """
        Удостоверяет, что БД и таблица созданы.
        """
        self._client.command(f'CREATE DATABASE IF NOT EXISTS {self._db_name}')

        self._client.command(f'''
            CREATE TABLE IF NOT EXISTS {self._db_name}.{self._table_name}
            (
                `patent_id` String,
                `title` String,
                `assignee` String,
                `authors` Array(String),
                `publications` Array(Date),
                `url` String,
                `classifications` Array(String),
                `abstract` String,
                `description` String,
                `claims` String,
                `cited_by` Array(String),
                `concepts` Nested
                (
                    `name` String,
                    `count` UInt16
                )
            )
            ENGINE = MergeTree
            PRIMARY KEY (organisation_name, patent_id)''')

    def check_if_patent_exists(self, patent_id: str) -> bool:
        """
        Проверяет, содержится ли патент в БД.
        :param patent_id: Уникальный идентификатор патента.
        :return: True, если содержится патент в БД, иначе - False.
        """
        result = self._client.query(
            f'SELECT 1 FROM {self._db_name}.{self._table_name} WHERE patent_id = {patent_id:String}',
            parameters={'patent_id': patent_id}
        )

        return result.row_count == 1

    def insert_patent(self, patent: PatentModel):
        """
        Добавляет патент в БД.
        :param patent: Патент.
        """
        self._client.insert(
            f'{self._db_name}.{self._table_name}',
            [[
                patent.patent_id,
                patent.title,
                patent.assignee,
                patent.authors,
                patent.publications,
                patent.url,
                patent.classifications,
                patent.abstract,
                patent.description,
                patent.claims,
                patent.cited_by,
                [concept['name'] for concept in patent.concepts],
                [concept['count'] for concept in patent.concepts]
            ]],
            column_names=[
                'patent_id',
                'title',
                'assignee',
                'authors',
                'publications',
                'url',
                'classifications',
                'abstract',
                'description',
                'claims',
                'cited_by',
                'concepts.name',
                'concepts.count'
            ])

    def get_table_for_analysis(self, limit: int = 500) -> pd.DataFrame:
        """
        Загружает термы всех патентов и устанавливает количество их употребления в каждом патенте.
        :param limit: Количество патентов в сформированном патентном массиве.
        :return: Таблица, содержащая количество употреблений термов в каждом патенте,
        дату публикации и количество цитирований.
        """
        df_info = self._client.query("""
            SELECT
              groupArray (patent_id) AS ids,
              groupArray (publications) as publications,
              groupUniqArrayArray (concepts.name) AS unique_concept_names
            FROM
              patent_analysis.patents
        """)

        df = pd.DataFrame(0, index=df_info.first_item['ids'], columns=df_info.first_item['unique_concept_names'])

        result = self._client.query("""
            SELECT
              patent_id,
              concepts.name,
              concepts.count,
              publications[-1],
              length(cited_by)
            FROM
              patent_analysis.patents
        """)

        for row in result.result_rows:
            for i in range(len(row[1])):
                df.at[row[0], row[1][i]] = row[2][i]

        # TODO: Подкорректировать во время работы.
        df['publication'] = result['publications']
        df['number_of_citations'] = result['length(cited_by)']

        return df

    def __del__(self) -> None:
        """
        Освобождает выделенные ресурсы.
        """
        self._client.close()

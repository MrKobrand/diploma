import pandas as pd
from pymongo import MongoClient

from Common.database_provider import DatabaseProvider
from Models.patent_model import PatentModel


class MongoProvider(DatabaseProvider):
    def __init__(self, connection_string: str) -> None:
        """
        Иницилизирует провайдер БД MongoDB.
        :param connection_string: Строка подключения к БД.
        """
        super().__init__(connection_string)
        self._client = MongoClient(self._connection_string)

    def ensure_db_and_table_created(self) -> None:
        """
        Удостоверяет, что БД и таблица созданы.
        """
        pass

    def check_if_patent_exists(self, patent_id: str) -> bool:
        """
        Проверяет, содержится ли патент в БД.
        :param patent_id: Уникальный идентификатор патента.
        :return: True, если содержится патент в БД, иначе - False.
        """
        return self._client[self._db_name][self._table_name].count_documents({'patent_id': patent_id}, limit=1) == 1

    def insert_patent(self, patent: PatentModel):
        """
        Добавляет патент в БД.
        :param patent: Патент.
        """
        self._client[self._db_name][self._table_name].insert_one({
            'patent_id': patent.patent_id,
            'title': patent.title,
            'assignee': patent.assignee,
            'authors': patent.authors,
            'publications': patent.publications,
            'url': patent.url,
            'classifications': patent.classifications,
            'abstract': patent.abstract,
            'description': patent.description,
            'cliams': patent.claims,
            'cited_by': patent.cited_by,
            'concepts': patent.concepts
        })

    def get_table_for_analysis(self, limit: int = 500) -> pd.DataFrame:
        """
        Загружает термы всех патентов и устанавливает количество их употребления в каждом патенте.
        :param limit: Количество патентов в сформированном патентном массиве.
        :return: Таблица, содержащая количество употреблений термов в каждом патенте,
        дату публикации и количество цитирований.
        """
        rows = self._client[self._db_name][self._table_name].find().limit(limit)

        ids = []
        df_rows = []

        for row in rows:
            ids.append(row['patent_id'])

            df_row = dict()

            for concept in row['concepts']:
                df_row[concept['name']] = concept['count']

            df_row['publication'] = row['publications'][-1]
            df_row['number_of_citations'] = len(row['cited_by'])
            df_row['organisation_name'] = row['assignee']

            df_rows.append(df_row)

        df = pd.DataFrame.from_dict(df_rows, orient='columns')
        df.fillna(0, inplace=True)
        df.index = ids

        return df.astype(float, errors='ignore')

    def __del__(self) -> None:
        """
        Освобождает выделенные ресурсы.
        """
        self._client.close()

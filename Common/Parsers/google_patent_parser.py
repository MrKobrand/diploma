from typing import List, Dict, Union

from bs4 import BeautifulSoup

from Common.WebDrivers.google_patent_webdriver import GooglePatentWebDriver
from Common.patent_parser import PatentParser


class GooglePatentParser(PatentParser):
    def __init__(self, google_patent_web_driver: GooglePatentWebDriver) -> None:
        self.__soup: BeautifulSoup = None
        self.__google_patent_web_driver = google_patent_web_driver

    def set_patent_link(self, patent_link: str) -> None:
        page = self.__google_patent_web_driver.get_patent_html(patent_link)
        self.__soup = BeautifulSoup(page, 'html.parser')

    def get_id(self) -> str:
        return self.__soup.find(id='pubnum').text

    def get_title(self) -> str:
        return self.__soup.find('h1', {'id': 'title'}).text.strip()

    def get_assignee(self) -> str:
        data_assignee = self.__soup.select_one('[data-assignee]')
        return data_assignee['data-assignee'] if data_assignee is not None else ''

    def get_authors(self) -> List[str]:
        return [data_inventor['data-inventor'] for data_inventor in self.__soup.select('[data-inventor]')]

    def get_publications(self) -> List[str]:
        return [publication_tag.text for publication_tag in self.__soup.find_all('div', {'class': 'publication'})]

    def get_abstract(self) -> str:
        return self.__soup.find('patent-text', {'name': 'abstract'}).text.strip()

    def get_description(self) -> str:
        return self.__soup.find('patent-text', {'name': 'description'}).text.strip()

    def get_claims(self) -> str:
        return self.__soup.find('patent-text', {'name': 'claims'}).text.strip()

    def get_classifications(self) -> List[str]:
        patent_classifications = []

        # 3 index is equal to the most nested div tag for single classification
        for classification_tag in self.__soup.find_all('div', {'class': 'classification-tree', 'hidden': False})[3::4]:
            patent_classifications.append(classification_tag.find_all('a', {'id': 'link'})[-1].text)

        return patent_classifications

    def get_cited_by(self) -> List[str]:
        cited_by_tag = self.__soup.find(id='citedBy')

        cited_by = []

        if cited_by_tag is not None:
            links = cited_by_tag.find_next('div', {'class': 'tbody'}).find_all('a', {'id': 'link'})
            cited_by = [link.text for link in links]

        return cited_by

    def get_concepts(self) -> List[Dict[str, Union[str, int]]]:
        concepts_tag = self.__soup.find(id='concepts')

        patent_concepts = []

        if concepts_tag is not None:
            for div_tag in concepts_tag.find_next('div', {'class': 'tbody'}).find_all('div', {'class': 'tr'}):
                concept_data = div_tag.find_all('span')

                # 2 index is equal to 'name', 5 index is equal to 'count'
                if concept_data[5].text.isdigit():
                    patent_concepts.append({'name': concept_data[2].text, 'count': int(concept_data[5].text)})

        return patent_concepts

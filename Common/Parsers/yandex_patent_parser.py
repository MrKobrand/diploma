from typing import List, Dict, Union

from bs4 import BeautifulSoup

from Common.WebDrivers.yandex_patent_webdriver import YandexPatentWebDriver
from Common.patent_parser import PatentParser


class YandexPatentParser(PatentParser):
    def __init__(self, yandex_patent_web_driver: YandexPatentWebDriver) -> None:
        self.__soup: BeautifulSoup = None
        self.__yandex_patent_web_driver = yandex_patent_web_driver

    def set_patent_link(self, patent_link: str) -> None:
        page = self.__yandex_patent_web_driver.get_patent_html(patent_link)
        self.__soup = BeautifulSoup(page, 'html.parser')

    def get_id(self) -> str:
        id_tags = self.__soup.find_all('div', {'class': 'doc-url-item'})
        return ''.join([id_tag.text for id_tag in id_tags]).replace(' ', '')

    def get_title(self) -> str:
        return self.__soup.find_all('div', {'class': 'document-title'}).pop().text.capitalize()

    def get_assignee(self) -> str:
        return self.__soup.find_all(lambda tag: 'Патентообладатели:' in tag.text).pop().next_sibling.text

    def get_authors(self) -> List[str]:
        return list(self.__soup.find_all(lambda tag: 'Авторы:' in tag.text).pop().next_sibling.stripped_strings)

    def get_publications(self) -> List[str]:
        request_date = self.__soup.find_all(lambda tag: 'Дата подачи заявки: ' in tag.text)\
            .pop().next_sibling.text.strip().replace('.', '-')

        patent_date = self.__soup.find_all(lambda tag: 'Опубликовано: ' in tag.text)\
            .pop().next_sibling.text.strip().replace('.', '-')

        return [request_date, patent_date]

    def get_abstract(self) -> str:
        abstract_tag = self.__soup.find(id='doc-abstract')

        abstract = ''

        if abstract_tag is not None:
            abstract = abstract_tag.next_sibling.text

        return abstract

    def get_description(self) -> str:
        description_tag = self.__soup.find(id='doc-description')

        description = ''

        if description_tag is not None:
            description = description_tag.next_sibling.text

        return description

    def get_claims(self) -> str:
        claims_tag = self.__soup.find(id='doc-claims')

        claims = ''

        if claims_tag is not None:
            claims = claims_tag.next_sibling.text

        return claims

    def get_classifications(self) -> List[str]:
        # 0 index is a title item
        mpk_items = self.__soup.find_all('div', {'class': 'header-mpk-item'})[1:]

        # 0 index equals ipc class, 1 index equals date
        return [mpk_item.contents[0].replace(' ', '') for mpk_item in mpk_items]

    def get_cited_by(self) -> List[str]:
        cited_by_tag = self.__soup.find(id='doc-table-cited-by')

        cited_by = []

        if cited_by_tag is not None:
            for cited_by_patent in cited_by_tag.find_next_sibling('div').find_all(
                    'div',
                    {'class': 'doctable_row_click'}
            ):
                cited_by.append(cited_by_patent.find_next('span').text)

        return cited_by

    def get_concepts(self) -> List[Dict[str, Union[str, int]]]:
        # YandexPatents не предоставляет список ключевых терминов патента
        return []

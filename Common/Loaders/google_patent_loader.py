from io import BytesIO
from typing import List

import pandas as pd
import requests

from Common.patent_loader import PatentLoader


class GooglePatentLoader(PatentLoader):
    def get_organisation_patent_links(self, search: str, is_organisation: bool = True) -> List[str]:
        main_patent_info_url = f'https://patents.google.com/xhr/query?url={"assignee" if is_organisation else "q"}' \
                               f'%3D{search}%26language%3DRUSSIAN&exp=&download=true'

        content = requests.get(main_patent_info_url).content

        # Удаление первой строки с 'search URL'
        return pd.read_csv(BytesIO(content[content.find(b'\n') + 1:]))['result link'].tolist()

from typing import List

from bs4 import BeautifulSoup

from Common.WebDrivers.yandex_patent_webdriver import YandexPatentWebDriver
from Common.patent_loader import PatentLoader


class YandexPatentLoader(PatentLoader):
    driver: YandexPatentWebDriver = None

    def __init__(self, yandex_patent_web_driver: YandexPatentWebDriver) -> None:
        super().__init__()
        self.driver = yandex_patent_web_driver

    def get_organisation_patent_links(self, search: str, is_organisation: bool = True) -> List[str]:
        patents_url = f'https://yandex.ru/patents?dco=RU&dco=SU&dl=ru' \
                      f'&{"dou" if is_organisation else "text"}={search}&spp=50'

        page = self.driver.get_search_patents(patents_url)
        soup = BeautifulSoup(page, 'html.parser')

        patent_links = soup.find_all('a', {'class': 'snippet-title'})

        return [f"https://yandex.ru{patent_link['href']}" for patent_link in patent_links]

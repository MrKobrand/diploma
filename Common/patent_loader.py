from abc import ABC, abstractmethod
from typing import List


class PatentLoader(ABC):
    @abstractmethod
    def get_organisation_patent_links(self, search: str, is_organisation: bool = True) -> List[str]:
        """
        Получить все ссылки на патенты.
        :param search: Поиск.
        :param is_organisation: Произвести поиск среди патентообладателей.
        :return: Список ссылок на патенты.
        """
        raise NotImplementedError

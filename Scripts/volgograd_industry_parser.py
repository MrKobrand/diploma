import re

import pandas as pd
import requests
from bs4 import BeautifulSoup

from utils import ROOT_DIR

URL = 'https://promtorg.volgograd.ru/current-activity/promyshlennost/list/map/'

VOLGOGRAD_INDUSTRY_FILE_PATH = f'{ROOT_DIR}/Data/volgograd_industry.csv'

ORGANISATION_COLUMN_NAME = 'Name of the organisation'


def main():
    page = requests.get(URL, verify=False)
    soup = BeautifulSoup(page.text, 'html.parser')
    root = soup.find_all('table', {'class': 'tableclass'})

    organisations = []

    for row in root[0].tbody.findAll('tr')[1:]:
        org_name = row.findAll('td')[0].contents[0]
        organisations.append(org_name)

    organisations = [re.sub('[«»",]', '', organisation) for organisation in organisations]

    pd.DataFrame(organisations, columns=[ORGANISATION_COLUMN_NAME]).to_csv(VOLGOGRAD_INDUSTRY_FILE_PATH, index=False)


if __name__ == '__main__':
    main()

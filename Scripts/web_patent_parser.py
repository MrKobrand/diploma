import re
import time
from datetime import datetime
from sys import platform

import pandas as pd

from Common.DatabaseProviders.clickhouse_provider import ClickhouseProvider
from Common.DatabaseProviders.mongo_provider import MongoProvider
from Common.Loaders.google_patent_loader import GooglePatentLoader
from Common.Parsers.google_patent_parser import GooglePatentParser
from Common.Parsers.yandex_patent_parser import YandexPatentParser
from Common.WebDrivers.google_patent_webdriver import GooglePatentWebDriver
from Common.WebDrivers.yandex_patent_webdriver import YandexPatentWebDriver
from Common.database import Database
from Common.patent import Patent
from Models.patent_model import PatentModel
from utils import CHROMEDRIVER_PATH, CLICKHOUSE_CONNECTION_STRING, MONGO_CONNECTION_STRING
from volgograd_industry_parser import VOLGOGRAD_INDUSTRY_FILE_PATH, ORGANISATION_COLUMN_NAME


def main():
    database_selection = int(
        input('Choose the DB to work with:\n'
              '\t1. ClickHouse;\n'
              '\t2. MongoDB.\n'
              '?: '))

    if database_selection == 1:
        if platform != 'linux' and platform != 'linux2':
            print('ClickHouse works only under Linux.')
            exit(1)
        database_provider = ClickhouseProvider(CLICKHOUSE_CONNECTION_STRING)
    elif database_selection == 2:
        database_provider = MongoProvider(MONGO_CONNECTION_STRING)
    else:
        print('Invalid menu item entered.')
        exit(2)

    database = Database(database_provider)

    database.ensure_db_and_table_created()

    google_driver = GooglePatentWebDriver(CHROMEDRIVER_PATH)
    google_loader = GooglePatentLoader()
    google_parser = GooglePatentParser(google_driver)

    yandex_driver = YandexPatentWebDriver(CHROMEDRIVER_PATH)
    yandex_parser = YandexPatentParser(yandex_driver)

    data_selection = int(
        input('Choose the source to extract data from:\n'
              '\t1. Volgograd Industry;\n'
              '\t2. Search by organisation name;\n'
              '\t3. Search by name;\n'
              '\t4. Local csv-file (must have \'result link\').\n'
              '?: '))

    if data_selection == 1:
        to_search = pd.read_csv(VOLGOGRAD_INDUSTRY_FILE_PATH)[ORGANISATION_COLUMN_NAME].tolist()
    elif data_selection == 2:
        organisation = input('Enter the organisation name: ')
        to_search = [organisation]
    elif data_selection == 3:
        name = input('Enter the name: ')
        to_search = [name]
    elif data_selection == 4:
        filename = input('Specify the path to the file: ')
        to_search = [filename]
    else:
        print('Invalid menu item entered.')
        exit(1)

    search_by_organisation = data_selection != 3

    patent_id_pattern = re.compile(r'^[A-Z]{2}[0-9]{6,7}[A-Z][0-9]$')

    for search in to_search:
        now = datetime.now().strftime('%H:%M:%S')
        print(f'{now} - [{to_search.index(search) + 1}/{len(to_search)}] - {search}')

        if data_selection != 4:
            patent_links = google_loader.get_organisation_patent_links(search, search_by_organisation)

            # Задержка, чтобы избежать бана 429 Too Many Requests
            time.sleep(15)
        else:
            try:
                patent_links = pd.read_csv(filename)['result link'].tolist()
            except KeyError:
                print('There is no column named \'result link\'.')
                exit(2)

        if not patent_links:
            print('There are no patent links.')

        for patent_link in patent_links:
            now = datetime.now().strftime('%H:%M:%S')
            print(f'\t{now} - [{patent_links.index(patent_link) + 1}/{len(patent_links)}] - {patent_link}')

            patent = Patent(patent_link, google_parser)

            patent_id = patent.get_id()

            if patent_id_pattern.match(patent_id):
                if not database.check_if_patent_exists(patent_id):
                    publications = patent.get_publications()
                    patent_classifications = patent.get_classifications()

                    if len(patent_classifications) == 0:
                        yandex_patent = Patent(
                            f'https://yandex.ru/patents/doc/{patent_id}_{publications[-1].replace("-", "")}',
                            yandex_parser
                        )

                        patent_classifications = yandex_patent.get_classifications()

                    patent_model = PatentModel()
                    patent_model.patent_id = patent_id
                    patent_model.title = patent.get_title()
                    patent_model.assignee = patent.get_assignee()
                    patent_model.authors = patent.get_authors()
                    patent_model.publications = [datetime.strptime(publication, '%Y-%m-%d')
                                                 for publication in patent.get_publications()]
                    patent_model.url = patent_link
                    patent_model.classifications = patent_classifications
                    patent_model.abstract = patent.get_abstract()
                    patent_model.description = patent.get_description()
                    patent_model.claims = patent.get_claims()
                    patent_model.cited_by = patent.get_cited_by()
                    patent_model.concepts = patent.get_concepts()

                    database.insert_patent(patent_model)

            # Задержка, чтобы избежать бана 429 Too Many Requests
            time.sleep(15)


if __name__ == '__main__':
    main()

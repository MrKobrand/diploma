import unittest

from Common.Parsers.google_patent_parser import GooglePatentParser
from Common.WebDrivers.google_patent_webdriver import GooglePatentWebDriver
from utils import CHROMEDRIVER_PATH

FULL_INFO_PATENT_LINK = 'https://patents.google.com/patent/RU2607281C2/ru'
NOT_FULL_INFO_PATENT_LINK = 'https://patents.google.com/patent/RU2019113434A3/ru'


class GooglePatentsParserTests(unittest.TestCase):

    def setUp(self) -> None:
        self.__google_driver = GooglePatentWebDriver(CHROMEDRIVER_PATH)
        self.__google_patent_parser = GooglePatentParser(self.__google_driver)

    def test_get_id(self):
        self.__google_patent_parser.set_patent_link(FULL_INFO_PATENT_LINK)

        expected_id = 'RU2607281C2'

        result_id = self.__google_patent_parser.get_id()

        self.assertEqual(expected_id, result_id)

    def test_get_title_correct(self):
        self.__google_patent_parser.set_patent_link(FULL_INFO_PATENT_LINK)

        expected_title = 'Система для погружения металлолома'

        result_title = self.__google_patent_parser.get_title()

        self.assertEqual(expected_title, result_title)

    def test_get_title_no_title(self):
        self.__google_patent_parser.set_patent_link(NOT_FULL_INFO_PATENT_LINK)

        expected_title = ''

        result_title = self.__google_patent_parser.get_title()

        self.assertEqual(expected_title, result_title)

    def tearDown(self) -> None:
        self.__google_driver.quit()


def main():
    unittest.main()


if __name__ == '__main__':
    main()

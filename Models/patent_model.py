from datetime import datetime
from typing import List, Dict, Union


class PatentModel:
    def __init__(self) -> None:
        self.__patent_id = ''
        self.__title = ''
        self.__assignee = ''
        self.__authors = []
        self.__publications = []
        self.__url = ''
        self.__abstract = ''
        self.__description = ''
        self.__claims = ''
        self.__classifications = []
        self.__cited_by = []
        self.__concepts = []

    @property
    def patent_id(self) -> str:
        """
        Получает уникальный идентификатор.
        :return: Уникальный идентификатор в строковом представлении.
        """
        return self.__patent_id

    @patent_id.setter
    def patent_id(self, patent_id: str) -> None:
        """
        Устанавливает уникальный идентификатор.
        :param patent_id: Уникальный идентификатор патента.
        """
        self.__patent_id = patent_id

    @property
    def title(self) -> str:
        """
        Получить заголовок.
        :return: Заголовок.
        """
        return self.__title

    @title.setter
    def title(self, title: str) -> None:
        """
        Устанавливает заголовок.
        :param title: Заголовок.
        """
        self.__title = title

    @property
    def assignee(self) -> str:
        """
        Получает правопреемника.
        :return: Правопреемник.
        """
        return self.__assignee

    @assignee.setter
    def assignee(self, assignee: str) -> None:
        """
        Устанавливает правопреемника.
        :param assignee: Правопреемник.
        """
        self.__assignee = assignee

    @property
    def authors(self) -> List[str]:
        """
        Получает авторов.
        :return: Список авторов.
        """
        return self.__authors

    @authors.setter
    def authors(self, authors: List[str]) -> None:
        """
        Устанавливает авторов.
        :param authors: Авторы.
        """
        self.__authors = authors

    @property
    def publications(self) -> List[datetime]:
        """
        Получает список дат публикаций.
        :return: Список дат.
        """
        return self.__publications

    @publications.setter
    def publications(self, publications: List[datetime]) -> None:
        """
        Устанавливает даты публикации.
        :param publications: Список дат.
        """
        self.__publications = publications

    @property
    def url(self) -> str:
        """
        Получает URL.
        :return: URL-ссылка.
        """
        return self.__url

    @url.setter
    def url(self, url: str) -> None:
        """
        Устанавливает URL.
        :param url: URL-ссылка.
        """
        self.__url = url

    @property
    def abstract(self) -> str:
        """
        Получает аннотацию.
        :return: Текст аннотации.
        """
        return self.__abstract

    @abstract.setter
    def abstract(self, abstract: str) -> None:
        """
        Устанавливает аннотацию.
        :param abstract: Текст аннотации.
        """
        self.__abstract = abstract

    @property
    def description(self) -> str:
        """
        Получает описание.
        :return: Текст описания.
        """
        return self.__description

    @description.setter
    def description(self, description: str) -> None:
        """
        Устнанавливает описание.
        :param description: Текст описания.
        """
        self.__description = description

    @property
    def claims(self) -> str:
        """
        Получает формулы изобретения.
        :return: Текст формул изобретения.
        """
        return self.__claims

    @claims.setter
    def claims(self, claims: str) -> None:
        """
        Устанавливает формулы изобретения.
        :param claims: Текст формул изобретения.
        """
        self.__claims = claims

    @property
    def classifications(self) -> List[str]:
        """
        Получает классы по классификации CPC.
        :return: Список названий классов.
        """
        return self.__classifications

    @classifications.setter
    def classifications(self, classifications: List[str]) -> None:
        """
        Устанавливает список классов по классификации СРС.
        :param classifications: Список названий классов.
        """
        self.__classifications = classifications

    @property
    def cited_by(self) -> List[str]:
        """
        Получает список идентификаторов патентов, цитирующих текущий патент.
        :return: Список идентификаторов патентов.
        """
        return self.__cited_by

    @cited_by.setter
    def cited_by(self, cited_by: List[str]) -> None:
        """
        Устанавливает список идентификаторов патентов, цитирующих текущий патент.
        :param cited_by: Список идентификаторов патентов.
        """
        self.__cited_by = cited_by

    @property
    def concepts(self) -> List[Dict[str, Union[str, int]]]:
        """
        Получает список ключевых терминов.
        :return: Список ключевых терминов с количеством употребления.
        """
        return self.__concepts

    @concepts.setter
    def concepts(self, concepts: List[Dict[str, Union[str, int]]]) -> None:
        """
        Устанавливает список ключевых терминов.
        :param concepts: Список ключевых терминов с количеством употребления.
        """
        self.__concepts = concepts
